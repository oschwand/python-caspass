#! /usr/bin/python

from http.cookiejar import MozillaCookieJar

from decouple import config

# Configuration

url = config("MOODLE_URL")
url_login = url + "/login/index.php"
url_success = url + "/my/"

# Authentification

from caspass import CASPass

cas = CASPass([url, url_login], url_success)
cas.login()
cas.save("cookies.txt")

# Authentified, do something

import httpx
from bs4 import BeautifulSoup

http = httpx.Client()
cookies = MozillaCookieJar()
cookies.load("cookies.txt", True, True)
http.cookies = cookies

r = http.get(url_success)
assert r.status_code == 200
soup = BeautifulSoup(r.content, "html.parser")

courses = soup.find(name="p", string="Mes cours").parent.find("ul")
for course in courses("a"):
    print(course.string, course["href"])
